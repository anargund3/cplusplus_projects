#! /usr/bin/python3
# encoding: utf-8

import os

top = '.'
out = 'build'

def options(opt):
    opt.load('compiler_cxx')

def configure(conf):
    conf.load('compiler_cxx')
    conf.env.append_value('CXXFLAGS', ['-std=c++11', '-g', '-Werror', '-Wall', 
                                        '-Wextra'])
    try:
        # cannot upload external folder to git, hence this
        conf.recurse('external')
    except:
        pass

def build(bld):
    dirs = next(os.walk(top))[1]
    for d in dirs:
        if d == 'build' or d[0] == '.':
            continue 
        bld.recurse(d)

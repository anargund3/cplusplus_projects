Abstract VM
Refer to this link for more details -
https://github.com/Binary-Hackers/42_Subjects/blob/master/00_Projects/07_CPP/abstract-vm.pdf

To run, go the top directory and run ./waf build --target=abstract_vm
And then run ./build/abstract_vm/abstract_vm input_file 
input_file is optional. You can enter the commands without the input file.
Refer to the linked pdf for syntax

TODO: Write unit tests ( :( )

#include "avm/operand.h"
#include "avm/avm_exception.h"
#include <limits>
#include <iostream>
namespace avm {
template<typename T>
void validate_add_inputs(T a, T b) {
    T sum = a + b;
    // check if a and b are of same sign
    if ((a < 0) == (b < 0)) {
        // if a is -ve but sum is greater than b raise Underflow error
        if (a < 0 && sum > b) {
           throw avm::Underflow(); 
        }
        // if a and b are +ve but sum is -ve
        else if (sum < b) {
            throw avm::Overflow();
        }
    }
}
template<typename T>
void valdiate_mul_inputs(T a, T b) {
    T max = std::numeric_limits<T>::max();
    T abs_a = (a < 0? -1*a : a);
    T abs_b = (b < 0? -1*b : b);
    if (abs_a > max/abs_b) {
        if ((a < 0) == (b < 0)) {
            // throw overflow
            throw avm::Overflow();
        }
        else {
            //throw underflow
            throw avm::Underflow();
        }
    }
}

template<typename T>
IOperand const *Operand<T>::operator+(IOperand const &rhs) const {
    if (this->getPrecision() < rhs.getPrecision()) {
        return (rhs + *this);
    }
    T lhs_val = static_cast<T>(std::stod(value));
    T rhs_val = static_cast<T>(std::stod(rhs.toString()));
    validate_add_inputs(lhs_val, rhs_val);
    T res = lhs_val + rhs_val;
    OperandFactory factory_;
    return factory_.createOperand(this->getType(), std::to_string(res)); 
}
template<typename T>
IOperand const *Operand<T>::operator-(IOperand const &rhs) const {
    OperandType out_type = this->getPrecision() > rhs.getPrecision() ? this->getType() : rhs.getType();
    double lhs_val = std::stod(value);
    double rhs_val = std::stod(rhs.toString());
    validate_add_inputs(lhs_val, -rhs_val);
    double res = lhs_val - rhs_val;
    OperandFactory factory_;
    return factory_.createOperand(out_type, std::to_string(res)); 
}
template<typename T>
IOperand const *Operand<T>::operator*(IOperand const &rhs) const {
    if (this->getPrecision() < rhs.getPrecision()) {
        return (rhs * *this);
    }
    T lhs_val = static_cast<T>(std::stod(value));
    T rhs_val = static_cast<T>(std::stod(rhs.toString()));
    valdiate_mul_inputs(lhs_val, rhs_val);
    T res = lhs_val * rhs_val;
    OperandFactory factory_;
    return factory_.createOperand(this->getType(), std::to_string(res)); 
}
template<typename T>
IOperand const *Operand<T>::operator/(IOperand const &rhs) const {
    OperandType out_type = this->getPrecision() > rhs.getPrecision() ? this->getType() : rhs.getType();
    auto lhs_val = std::stod(value);
    auto rhs_val = std::stod(rhs.toString());
    if (rhs_val == 0) {
        throw avm::DivideByZero();
    }
    auto res = lhs_val / rhs_val;
    OperandFactory factory_;
    return factory_.createOperand(out_type, std::to_string(res)); 
}
template<typename T>
IOperand const *Operand<T>::operator%(IOperand const &rhs) const {
    OperandType out_type = this->getPrecision() > rhs.getPrecision() ? this->getType() : rhs.getType();
    auto lhs_val = std::stol(value);
    auto rhs_val = std::stol(rhs.toString());
    if (rhs_val == 0) {
        throw avm::DivideByZero();
    }
    auto res = lhs_val % rhs_val;
    OperandFactory factory_;
    return factory_.createOperand(out_type, std::to_string(res)); 
}

template<>
int Operand<int8_t>::getPrecision() const {
    return Int8; 
}

template<>
OperandType Operand<int8_t>::getType() const {
    return Int8;
}

template<>
int Operand<int16_t>::getPrecision() const {
    return Int16; 
}

template<>
OperandType Operand<int16_t>::getType() const {
    return Int16;
}

template<>
int Operand<int32_t>::getPrecision() const {
    return Int32; 
}

template<>
OperandType Operand<int32_t>::getType() const {
    return Int32;
}

template<>
int Operand<float>::getPrecision() const {
    return Float; 
}

template<>
OperandType Operand<float>::getType() const {
    return Float;
}

template<>
int Operand<double>::getPrecision() const {
    return Double; 
}

template<>
OperandType Operand<double>::getType() const {
    return Double;
}

template class Operand<int8_t>;
template class Operand<int16_t>;
template class Operand<int32_t>;
template class Operand<float>;
template class Operand<double>;

}
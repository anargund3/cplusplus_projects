#include "avm/rpn_evaluator.h"

#include <iostream>
#include <string>
#include <fstream>

int main(int argc, char* argv[]) {
    
    std::string line;
    std::string code;
    auto rpn = avm::RPNEvaluator();
    
    // for formatting error messages
    std::string pre =  "\033[1;31m";
    std::string post = "\033[0m\n";
    std::string filename;
    if (argc == 1) {
        std::cout << "-- Abstract VM --\n" << "Enter your commands\n";
        std::getline(std::cin, line);
        while (line != ";;") {
            code += (line + '\n');
            std::getline(std::cin, line);
        } 
    }
    else if (argc == 2) {
        filename = argv[1];
        if (filename.empty() || filename.find(".avm") == std::string::npos) {
            std::cout << pre + "Invalid file input" + post;
            return 1;
        }
        std::ifstream file_in;
        file_in.open(filename, std::ifstream::in);
        if (!file_in.is_open()) {
            std::cout << pre + "Unable to open file" + post;
            return 1;
        }
        while (!file_in.eof()) {
            std::getline(file_in, line);
            code += (line + '\n');
        }
    }
    else {
        std::cout << "Invalid arguments\n";
        std::cout << "Run as ./build/abstract_vm/abstract_vm [filename]\n";
    }
    if (code.empty()) {
        std::cout << pre + "Unable to obtain code, exiting..." + post;
        return 1;
    }

    std::size_t pos;
    std::size_t lineno = 0;
    while((pos = code.find('\n')) != std::string::npos) {
        auto cmd = code.substr(0, pos);
        code.erase(0, pos+1);
        lineno++;
        if(!rpn(cmd) && cmd != "exit") {
            if (filename.empty()) {
                std::cout << "At line (" + std::to_string(lineno) + ") " 
                           << cmd + '\n';
                return 1;
            }
            std::cout << "At [" + filename + "(" + std::to_string(lineno) + ")] "
                      << cmd + '\n';
            return 1;
        }
    }
    return 0;
}
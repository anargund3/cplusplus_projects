#include "avm/rpn_evaluator.h"
#include "avm/operand_factory.h"
#include "avm/avm_exception.h"

#include <iostream>
#include <algorithm>
namespace avm {

bool RPNEvaluator::operator()(std::string &cmd) {
    
    try {
        if(cmd.find(';') != std::string::npos) {
            cmd.erase(cmd.find(';'), std::string::npos);
        }
        if (cmd.find_first_not_of(" \t") != 0) {
            cmd.erase(0, cmd.find_first_not_of(" \t"));
        }
	    while (!cmd.empty() && cmd.find_last_of(" \t") == cmd.size()-1) {
            cmd.pop_back();
        }
        if (cmd.empty()) {
            return true;
        }
        auto wpos = cmd.find(' ', 0);
        auto cmd_type = cmd.substr(0, wpos);
        if(cmd_type == "push") {
            std::string operand = cmd.substr(wpos+1, std::string::npos);
            this->push(operand);
        }
        else if (cmd_type == "pop") {
            this->pop();
        }
        else if (cmd_type == "assert") {
            std::string operand = cmd.substr(wpos+1, std::string::npos);
            this->assert(operand);
        }
        else if (cmd_type == "dump") {
            for (auto it = expr_stack.rbegin(); it != expr_stack.rend(); it++) {
                std::cout << (*it)->toString().c_str() << std::endl;
            }
        }
        else if (cmd_type == "print") {
            this->print();
        }
        else if (cmd_type == "exit") {
            this->expr_stack.clear();
            return false;
        }
        else {
            if (this->expr_stack.size() < 2) {
                throw avm::InsufficientOps();
            }
            auto rhs = this->expr_stack.back();
            this->pop();
            auto lhs = this->expr_stack.back();
            this->pop();
            const IOperand* res = nullptr;
            if (cmd_type == "add") {
                res = *lhs + *rhs;
            }
            if (cmd_type == "sub") {
                res = *lhs - *rhs;
            }
            if (cmd_type == "mul") {
                res = *lhs * *rhs;
            }
            if (cmd_type == "div") {
                res = *lhs / *rhs;
            }
            if (cmd_type == "mod") {
                res = *lhs % *rhs;
            }
            if (!res) {
                throw avm::InvalidInstr();
            }
            this->push(res->getType(), res->toString());
        }
    }
    catch (std::exception &ex) {
        std::string pre =  "\033[1;31m";
        std::string post = "\033[0m\n";
        std::string err_msg = pre + "Error: " + ex.what() + post;
        std::cout << err_msg;
        return false;
    } 
    return true;
}

void RPNEvaluator::push(OperandType type, const std::string &value) {
    this->expr_stack.push_back(factory_.createOperand(type, value));
}
void RPNEvaluator::push(const std::string& operand) {

    if (operand.empty()) {
        throw avm::InvalidOp();
    }
    auto open = operand.find('(', 0);
    auto close = operand.find(')', 0);
    if (open == std::string::npos || close == std::string::npos || close < open) {
        throw avm::InvalidOp();
    }
    std::string operand_type = operand.substr(0, open);
    if (std::find(operand_names.begin(), operand_names.end(), operand_type) == operand_names.end()) {
        throw avm::InvalidOp();
    }
    std::string val = operand.substr(open+1, close-open-1);
    if (std::find_if(val.begin(), val.end(),[](const char c) {
        return !isdigit(c) && c != '.' && c != '+' && c != '-';
    } ) != val.end()) {
        throw avm::InvalidValue();
    }
    expr_stack.push_back(factory_.createOperand(name_to_type_map.at(operand_type), val));
}

void RPNEvaluator::pop() {
    if(expr_stack.empty()) {
        throw avm::PopOnEmpty();
    }
    expr_stack.pop_back();
}

void RPNEvaluator::assert(const std::string& operand) {

    if (operand.empty()) {
        throw avm::InvalidOp();
    }
    auto open = operand.find('(', 0);
    auto close = operand.find(')', 0);
    if (open == std::string::npos || close == std::string::npos || close < open) {
        throw avm::InvalidOp();
    }
    std::string operand_type = operand.substr(0, open);
    if (std::find(operand_names.begin(), operand_names.end(), operand_type) == operand_names.end()) {
        throw avm::InvalidOp();
    }
    std::string val = operand.substr(open+1, close-open-1);
    if (std::find_if(val.begin(), val.end(),[](const char c) {
        return !isdigit(c) && c != '.' && c != '+' && c != '-';
    } ) != val.end()) {
        throw avm::InvalidValue();
    }
    auto top_val = expr_stack.back();
    bool out = top_val->getType() == name_to_type_map.at(operand_type) 
            && top_val->toString() == val;
    if (out) {
        std::cout << "true\n";
        return;
    }
    throw avm::AssertFalse();
}

void RPNEvaluator::print() {

    auto top = this->expr_stack.back();
    if (top->getType() != Int8) {
        throw avm::NotInt8();
    }
    auto val = top->toString();
    int8_t out_val = static_cast<int8_t>(std::stoi(val));
    std::cout << "Top value is " << out_val << std::endl;
}

}
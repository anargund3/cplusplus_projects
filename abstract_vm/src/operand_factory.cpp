#include "avm/operand_factory.h"
#include "avm/operand.h"

namespace avm {

using OperandInt8 = Operand<int8_t>;
using OperandInt16 = Operand<int16_t>;
using OperandInt32 = Operand<int32_t>;
using OperandFloat = Operand<float>;
using OperandDouble = Operand<double>;

IOperand const *OperandFactory::createOperand(OperandType type, const std::string &value) {
    return this->creators[type](value);
}

IOperand const *OperandFactory::createInt8(const std::string &value) {
    IOperand *out = new OperandInt8(value);
    return out;
}
IOperand const *OperandFactory::createInt16(const std::string &value) {
    IOperand *out = new OperandInt16(value);
    return out;
}
IOperand const *OperandFactory::createInt32(const std::string &value) {
    IOperand *out = new OperandInt32(value);
    return out;
}
IOperand const *OperandFactory::createFloat(const std::string &value) {
    IOperand *out = new OperandFloat(value);
    return out;
}
IOperand const *OperandFactory::createDouble(const std::string &value) {
    IOperand *out = new OperandDouble(value);
    return out;
}

} // namespace avm

#pragma once
#include "avm/ioperand.h"
#include "avm/operand_factory.h"

#include <vector>
#include <unordered_map>

namespace avm {
const std::array<std::string, OperandFactory::kNumOperands> operand_names = 
                        { "int8", "int16", "int32", "float", "double" };     

const std::unordered_map<std::string, OperandType> name_to_type_map({
  {"int8", OperandType::Int8}, 
  {"int16", OperandType::Int16},
  {"int32", OperandType::Int32},
  {"float", OperandType::Float},
  {"double", OperandType::Double}
});
class RPNEvaluator {
  public:
    RPNEvaluator(){
    }
    bool operator()(std::string &cmd);
    void push(const std::string &operand);
    void push(OperandType type, const std::string &value);
    void pop();
    void dump();
    void print();
    void assert(const std::string &operand);
    void exit();
  private:
    std::vector<const IOperand*> expr_stack;
    OperandFactory factory_;
};

} //namespace avm
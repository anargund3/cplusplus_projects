#pragma once 
#include <exception>

namespace avm {

struct Overflow : public std::exception {
    const char* what() const noexcept {
        return "Overflow detected";
    }
};

struct Underflow : public std::exception {
    const char* what() const noexcept {
        return "Underflow detected";
    }
};

struct DivideByZero : public std::exception {
    const char* what() const noexcept {
        return "Divide-by-zero detected";
    }
};

struct InvalidOp : public std::exception {
    const char* what() const noexcept {
        return "Invalid operation";
    }
};
struct InvalidInstr : public std::exception {
    const char* what() const noexcept {
        return "Invalid instruction";
    }
};
struct InvalidValue : public std::exception {
    const char* what() const noexcept {
        return "Invalid value";
    }
};

struct NotInt8 : public std::exception {
    const char* what() const noexcept {
        return "Operand not of type Int8";
    }
};

struct AssertFalse : public std::exception {
    const char* what() const noexcept {
        return "Assert return false";
    }
};

struct PopOnEmpty : public std::exception {
    const char* what() const noexcept {
        return "Trying to pop from empty stack";
    }
};
struct InsufficientOps : public std::exception {
    const char* what() const noexcept {
        return "Not enough values in stack for operation";
    }
};
} // namespace avm
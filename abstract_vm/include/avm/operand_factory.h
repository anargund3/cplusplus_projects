#pragma once
#include "avm/ioperand.h"

#include <functional>
#include <array>

namespace avm {

class OperandFactory {

  public:
    static constexpr int kNumOperands = 5;
    OperandFactory() {
      creators[OperandType::Int8] = std::bind(&OperandFactory::createInt8, this, std::placeholders::_1);
      creators[OperandType::Int16] = std::bind(&OperandFactory::createInt16, this, std::placeholders::_1);
      creators[OperandType::Int32] = std::bind(&OperandFactory::createInt32, this, std::placeholders::_1);
      creators[OperandType::Float] = std::bind(&OperandFactory::createFloat, this, std::placeholders::_1);
      creators[OperandType::Double] = std::bind(&OperandFactory::createDouble, this, std::placeholders::_1);
    }
    IOperand const *createOperand(OperandType type, const std::string &value);
  private:
    IOperand const *createInt8(const std::string &value);
    IOperand const *createInt16(const std::string &value);
    IOperand const *createInt32(const std::string &value);
    IOperand const *createFloat(const std::string &value);
    IOperand const *createDouble(const std::string &value);
    std::array<std::function<const IOperand*(std::string)>, kNumOperands> creators; 

};
} // namespace avm

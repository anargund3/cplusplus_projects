#pragma once
#include "avm/ioperand.h"
#include "avm/operand_factory.h"

namespace avm {
template <typename T>
class Operand : public IOperand {
  public:
    Operand() = delete;
    Operand(T data) : value(std::to_string(data)) {}
    Operand(std::string val) : value(std::to_string(static_cast<T>(std::stod(val)))) {}
    int getPrecision() const override;
    OperandType getType() const override; 
    std::string const &toString() const override {
        return value;
    }

    IOperand const *operator+(IOperand const &rhs) const override;
    IOperand const *operator-(IOperand const &rhs) const override;
    IOperand const *operator*(IOperand const &rhs) const override;
    IOperand const *operator/(IOperand const &rhs) const override;
    IOperand const *operator%(IOperand const &rhs) const override;
    
  protected:
    std::string value;
};

} //namespace avm